# coding: utf-8
import pandas as pd
import json
import datetime
import re
import sys
from sklearn.cluster import KMeans
import numpy as np
from contextlib import contextmanager
from pylab import plot,show
from numpy import vstack,array
from numpy.random import rand
from scipy.cluster.vq import kmeans,vq
from sklearn import preprocessing

# # Define functions

@contextmanager
def stdout_redirected(new_stdout):
    save_stdout = sys.stdout
    sys.stdout = new_stdout
    try:
        yield None
    finally:
        sys.stdout = save_stdout
    
def cleaning(jsondata1): 
    # drop 5 lines with noise information
    a1 = jsondata1.splitlines()[0:4200]
    a2 = jsondata1.splitlines()[4201:9201]
    a3 = jsondata1.splitlines()[9202:13502]
    a4 = jsondata1.splitlines()[13503:17703]
    a5 = jsondata1.splitlines()[17704:21704]
    a6 = jsondata1.splitlines()[21705:25880]

    a = a1+a2+a3+a4+a5+a6

    with open('order11.json', "w") as f:
        with stdout_redirected(f):
            for i in range(len(a)):
                     print a[i]
            
    b = []
    for line in open('order11.json', 'r'):
        b.append(json.loads(line))
       
    # choose three parameters: total sum of check, customer id, date of buing
    total = []
    for i in range(len(b)):
        total.append(b[i]['total'])
    
    customer = []
    for i in range(len(b)):
        customer.append(b[i]['customer'])
    
    date = []
    for i in range(len(b)):
        date.append(b[i]['created'])
    
    # create Data Frame
    df = pd.DataFrame()
    
    df.loc[:, 'customer']=customer
    df.loc[:, 'date']=date
    df.loc[:, 'total']=total

    return df

def sum_col(col):
    S = 0
    for n in col:
        S = S + n
    return S

def plot_metrics(cls, points, metrics_list, xaxis = 0, yaxis = 1,plt_name = 'plt'):
    labels = cls.labels_
    cluster_centers = cls.cluster_centers_
    labels_unique = np.unique(labels)
    n_clusters_ = len(labels_unique)

    import matplotlib.pyplot as plt
    from itertools import cycle

    plt.figure(1,  figsize=(8, 8))
    plt.clf()
    # setting x,y axis according to mapping list of metrics
    plt.xlabel(metrics_list[xaxis])
    plt.ylabel(metrics_list[yaxis])

    colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
    for k, col in zip(range(n_clusters_), colors):
        my_members = labels == k
        cluster_center = cluster_centers[k]

        plt.plot(points[my_members, xaxis], points[my_members, yaxis], col + '.')
        plt.plot(cluster_center[xaxis], cluster_center[yaxis], 'o', markerfacecolor=col,
                     markeredgecolor='k', markersize=14)
    plt.title('Clusters: %d' % n_clusters_)
    fname = plt_name+'.png'
    plt.savefig(fname)
    
class Ecommerce_step3:
    def __init__(self,f,t = "temp_file_with_data.csv"):
        self.fileName = f
        self.tmpFile = t

    def LoadAndPreapereData(self):
        # # Read and clean data
        # clean data
        with open(self.fileName, "rb") as f:
            # read the entire input; in a real application,
            # you would want to read a chunk at a time
            bsondata = f.read()
            jsondata = re.sub(r'ObjectId\s*\(\s*\"(\S+)\"\s*\)',
                              r'"\1"',
                              bsondata)
            jsondata = re.sub(r'Date\s*\(\s*(\S+)\s*\)',
                              r'"\1"',
                              jsondata)
            jsondata1 = re.sub(r'"\\"TalkTalk\\""',
                              r'"TalkTalk"',
                              jsondata)

        l = len(jsondata1.splitlines())
        m = int(l/4000)
        print '--------------------------------------------------'
        print '------------------Data Loading--------------------'
        print '--------------------------------------------------'
        self.df = cleaning(jsondata1)
        self.df.to_csv(self.tmpFile, index=False)
        self.df = pd.read_csv(self.tmpFile)
        
        self.__data_analysis__();
        self.__customer_lifespan__();
        self.__lifeTime_value_parameters__();
        #self.__k_means_clustering__();
        #self.__data_normalization__();
        self.__clustering_with_additional_features();
        
        
        
    def __data_analysis__(self):
        df2 = self.df[["customer", "total"]]

        # find general sum of purchase for each customer by all period

        df3 = df2.groupby(by=['customer'])['total'].sum()

        df3.sort(["customer"])

        col1 = df3.index
        col2 = df3.values
        print '--------------------------------------------------'
        print '-----------------Data Analysis--------------------'
        print '--------------------------------------------------'
        self.general = pd.DataFrame()

        self.general.loc[:, "customer"]=col1
        self.general.loc[:, "total"]=col2

        # find number of purchases for all period
        df5 = self.df.groupby(by=['customer'])['date'].count()
        df5 = df5.sort_index()

        self.general = self.general.sort(['customer'])
        self.general.loc[:, "buy"]=df5.values


    def __customer_lifespan__(self):
        # find date of the first purchase
        df_max = self.df.groupby(by=['customer'])['date'].max()
        self.general.loc[:, 'date_max']=df_max.values

        # find date of the last purchase
        df_min = self.df.groupby(by=['customer'])['date'].min()
        self.general.loc[:, 'date_min']=df_min.values

        self.general.index = range(len(self.general['customer']))
        print '--------------------------------------------------'
        print '----------------LTV calculating-------------------'
        print '--------------------------------------------------'
        self.general.loc[:, 'date_max'] = self.general['date_max']/ 1000
        self.general.loc[:, 'date_min'] = self.general['date_min']/ 1000

        self.general.loc[:, 'days']=0

        # find period of life of customer in shop (in days)
        for i in xrange(len(self.general['date_max'])):
                d = datetime.datetime.fromtimestamp(self.general['date_max'][i])-datetime.datetime.fromtimestamp(self.general['date_min'][i])
                self.general.loc[i, 'days'] = d.days+1

        self.general = self.general[['customer', 'total', 'buy', 'days']]

    def __lifeTime_value_parameters__(self):
        # find the first and the last date in dataset
        first = datetime.datetime.fromtimestamp(self.df['date'][0]/1000)
        last = datetime.datetime.fromtimestamp(self.df['date'][len(self.df['date'])-1]/1000)

        period = last - first
        week_number = period.days/7

        # find average customer value per week 
        self.general.loc[:, 'mean'] = self.general['total']/self.general['buy']
        self.general.loc[:, 'mean_week'] = self.general['total']/week_number

        df_ltv = self.general[['customer', 'mean', 'mean_week']]

        # find average number of visits per week (purchase cycle)
        df_ltv.loc[:, "cycle_per_week"] = self.general['buy']/week_number

        # find the average customer lifwspan (how long someone remains  customer) in years
        df_ltv.loc[:, 'year'] = self.general['days']/365

        # find profit per customer (in %)
        general_profit = sum_col(self.general['total'])
        df_ltv.loc[:, 'profit'] = self.general['total']/general_profit

        df_ltv.loc[:, 'LTV_simple']=52*df_ltv['mean_week']*df_ltv['year']

        df_ltv.loc[:, 'LTV_custom']=df_ltv['year']*(52*df_ltv['mean']*df_ltv['cycle_per_week']*df_ltv['profit'])

        df_ltv.loc[:, 'LTV']=(df_ltv['LTV_simple'] + df_ltv['LTV_custom'])/2

        self.ltv = df_ltv[['customer', 'LTV']]

        self.ltv.loc[:, 'days']=self.general['days']

        self.ltv = self.ltv.sort(['LTV', 'days'])
        self.ltv.index=range(len(self.ltv['LTV']))

    
    def __clustering_with_additional_features(self):
        self.ltv_add = self.ltv.sort(['customer'])

        ltv_prob = self.general[['total', 'buy', 'days']]
        ltv_prob.loc[:, 'LTV']=self.ltv['LTV']

        self.data_add = ltv_prob.values

        min_max_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
        self.data_add = min_max_scaler.fit_transform(self.data_add)

        self.metrics_list_add = ['LTV', 'days', 'total', 'buy']

    def KMeansPlottingWADF(self):
        
        for cl in [4]:
            cls = KMeans(n_clusters=cl, init='k-means++', 
            max_iter=100, n_init=10, verbose=0, random_state=3425)
            cls.fit(self.data_add)

            labels = cls.labels_
            cluster_centers = cls.cluster_centers_

            labels_unique = np.unique(labels)
            n_clusters_ = len(labels_unique)

            

            for i in range(len(self.metrics_list_add)):
                for j in range(len(self.metrics_list_add)):
                    if i <= j:
                        pass
                    else:
                        plt_name = 'plt_3_'+str(i)
                        plot_metrics(cls, self.data_add, self.metrics_list_add, i,j)

    
    def FinalTable(self,fileName,k = 4):
        data=self.ltv_add[['LTV', 'days']].values
        min_max_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
        data = min_max_scaler.fit_transform(data)

        # computing K-Means with K = 4 (4 clusters)
        cls = KMeans(n_clusters=4, init='k-means++', max_iter=1000, n_init=1, verbose=0, random_state=3425)
        # assign each sample to a cluster
        cls.fit(data)		
		
        print '--------------------------------------------------'
        print '------------------Results saving------------------'
        print '--------------------------------------------------'
        self.ltv_add.loc[:, 'group']=cls.labels_


        self.ltv_add = self.ltv_add[['customer', 'LTV', 'group']]

        self.ltv_add.to_csv(fileName, index=False)

if __name__ == '__main__':    
    e = Ecommerce_step3("order.json")
    e.LoadAndPreapereData()
    #e.KMeansPlottingWADF()
    e.FinalTable('LTV+cluster.csv')
    print '--------------------------------------------------'
    print '------------------JOB IS DONE---------------------'
    print '--------------------------------------------------'
    print '--------------File LTV+cluster.csv----------------'
    print '--------------------is saved----------------------'
    print '--------------------------------------------------'