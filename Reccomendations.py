# coding: utf-8

from Ecommerce import Ecommerce

if __name__ == '__main__':    
    e = Ecommerce("mongodb://127.0.0.1:27017/")	
    e.LoadAndPreapereData()
    print '--------------------------------------------------'
    print '-------------Similarity calculating---------------'
    print '--------------------------------------------------'
    with open('products_list.txt', 'rU') as in_file:
        list_p = in_file.read().split('\n')
    customer = list_p[0]
    list_prod = list_p[1:]
    print 'Customer ', customer
    print 'List of products ', list_prod
    e.RecommendProducts(list_prod)
    print '--------------------------------------------------'
    print '-----------Reccomendations building---------------'
    print '--------------------------------------------------'
    #e.RecommendedForYou(customer)
    print '--------------------------------------------------'
    print '------------------JOB IS DONE---------------------'
    print '--------------------------------------------------'
    print '--------Results are saved to the files------------'
    print '--------------------------------------------------'
    print '------------RecommendedForYou.json----------------'
    print '------------RecommendProducts.json----------------'
    print '--------------------------------------------------'