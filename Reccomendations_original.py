# coding: utf-8

import pandas as pd
import json
import datetime
import re
from math import sqrt
import sys
import argparse
from sklearn.cluster import KMeans
import numpy as np
from contextlib import contextmanager
from itertools import groupby
from pylab import plot,show
import collections
from numpy import vstack,array
from numpy.random import rand
from scipy.cluster.vq import kmeans,vq

@contextmanager
def stdout_redirected(new_stdout):
    save_stdout = sys.stdout
    sys.stdout = new_stdout
    try:
        yield None
    finally:
        sys.stdout = save_stdout
        
def unique(s):
    
    n = len(s)
    if n == 0:
        return []
   
    u = {}
    try:
        for x in s:
            u[x] = 1
    except TypeError:
        del u  # move on to the next method
    else:
        return u.keys()
   
    try:
        t = list(s)
        t.sort()
    except TypeError:
        del t  # move on to the next method
    else:
        assert n > 0
        last = t[0]
        lasti = i = 1
        while i < n:
            if t[i] != last:
                t[lasti] = last = t[i]
                lasti += 1
            i += 1
        return t[:lasti]

    # Brute force is all that's left.
    u = []
    for x in s:
        if x not in u:
            u.append(x)
    return u

def cleaning(jsondata1): 
      
    # drop 5 lines with noise information
    a1 = jsondata1.splitlines()[0:4200]
    a2 = jsondata1.splitlines()[4201:9201]
    a3 = jsondata1.splitlines()[9202:13502]
    a4 = jsondata1.splitlines()[13503:17703]
    a5 = jsondata1.splitlines()[17704:21704]
    a6 = jsondata1.splitlines()[21705:25880]

    a = a1+a2+a3+a4+a5+a6
    with open('order11.json', "w") as f:
        with stdout_redirected(f):
            for i in range(len(a)):
                        print a[i]
            
    b = []
    for line in open('order11.json', 'r'):
        b.append(json.loads(line))
        
    # choose three parameters: total sum of check, customer id, date of buing
    total = []
    for i in range(len(b)):
        total.append(b[i]['total'])
    print '--------------------------------------------------'
    print '-------------------Data reading-------------------'
    print '--------------------------------------------------'
    sub_total = []
    for i in range(len(b)):
        sub_total.append(b[i]['sub_total'])
    

    customer = []
    for i in range(len(b)):
        customer.append(b[i]['customer'])
    
    date = []
    for i in range(len(b)):
        date.append(b[i]['created'])
    
    tax = []
    for i in range(len(b)):
        tax.append(b[i]['tax'])
        
    items1 = []
    for i in range(len(b)):
        if b[i]['total'] > 0:
            items1.append(b[i]['items'])
        else:
            items1.append('0')

    items2 = []
    for i in  range(len(items1)):
        name = []
        for j in range(len(items1[i])):
            if items1[i][j] > 0 and items1[i] != '0':            
                name.append(items1[i][j]['description'])
        items2.append(name)
        
    items3 = []
    for i in  range(len(items1)):
        name = []
        for j in range(len(items1[i])):
            if items1[i][j] > 0 and items1[i] != '0':            
                name.append(items1[i][j]['itemkey'])
        items3.append(name)
                                                                         
    items4 = []
    for i in  range(len(items1)):
        name = []
        for j in range(len(items1[i])):
            if items1[i][j] > 0 and items1[i] != '0':            
                name.append(items1[i][j]['total_price'])
        items4.append(name)
  
    items5 = []
    for i in  range(len(items1)):
        name = []
        for j in range(len(items1[i])):
            if items1[i][j] > 0 and items1[i] != '0':            
                name.append(items1[i][j]['qty'])
        items5.append(name)
        
    items6 = []
    for i in  range(len(items1)):
        name = []
        for j in range(len(items1[i])):
            if items1[i][j] > 0 and items1[i] != '0':            
                name.append(items1[i][j]['productsku'])
        items6.append(name)
    
    
    
    print(len(total))
    print(len(sub_total))
    print(len(customer))
    print(len(date))
    print(len(tax))
    print(len(items2))
    print(len(items3))
    print(len(items4))
    print(len(items5))
    print(len(items6))
    
    # create Data Frame
    df = pd.DataFrame()
    
    df['customer']=customer
    df['date']=date
    df['total']=total
    df['sub_total'] =sub_total
    df['tax']=tax
    df['items_key']=items3
    df['item_price']=items4
    df['productsku']=items6
    df['item_qty']=items5
    df['items_descr']=items2

    return df, customer, items2, items6

def sum_col(col):
    S = 0
    for n in col:
        S = S + n
    return S

def sim_distance(prefs,person1,person2):
    
    # Get the list of shared_items
    si={}
    for item in prefs[person1]:
        if item in prefs[person2]:
            si[item]=1
            
    # if they have no purchase in common, return 0
    if len(si)==0: return 0

    # Add up the squares of all the differences
    sum_of_squares=sum([pow(prefs[person1][item]-prefs[person2][item],2) for item in prefs[person1] if item in prefs[person2]])
    return 1.0/(1.0+float(sum_of_squares))

# Returns the Pearson correlation coefficient for p1 and p2
def sim_pearson(prefs,p1,p2):
    # Get the list of mutually rated items
    si={}
    for item in prefs[p1]:
        if item in prefs[p2]:
            si[item]=1
            
    # Find the number of elements
    n=len(si)
    
    # if they are no purchase in common, return 0
    if n==0: return 0
    
    # Add up all the preferences
    sum1=sum([prefs[p1][it] for it in si])
    sum2=sum([prefs[p2][it] for it in si])
    
    # Sum up the squares
    sum1Sq=sum([pow(prefs[p1][it],2) for it in si])
    sum2Sq=sum([pow(prefs[p2][it],2) for it in si])
    
    # Sum up the products
    pSum=sum([prefs[p1][it]*prefs[p2][it] for it in si])
    
    # Calculate Pearson score
    num=pSum-(sum1*sum2/n)
    den=sqrt((sum1Sq-pow(sum1,2)/n)*(sum2Sq-pow(sum2,2)/n))
    
    if den==0: return 0
    r=num/den
    return r

# Returns the best matches for person from the prefs dictionary.
# Number of results and similarity function are optional params.
def topMatches(prefs,person,n=5,similarity=sim_pearson):
    scores=[(similarity(prefs,person,other),other)  for other in prefs if other!=person]
    
    # Sort the list so the highest scores appear at the top
    scores.sort( )
    scores.reverse( )
    return scores[0:n]

# Gets recommendations for a person by using a weighted average
# of every other user's rankings
def getRecommendations(prefs,person,similarity=sim_pearson):
    totals={}
    simSums={}
    print '--------------------------------------------------'
    print '-----------Reccomandations building---------------'
    print '--------------------------------------------------'
    for other in prefs:
        
        # don't compare me to myself
        if other==person: 
            continue 
            
        sim=similarity(prefs,person,other)
        
        # ignore scores of zero or lower
        if sim<=0: 
            continue
            
        for item in prefs[other]:
            # only score movies I haven't seen yet
            
            if item not in prefs[person] or prefs[person][item]==0:
                
                # Similarity * Score
                totals.setdefault(item,0)
                totals[item]+=prefs[other][item]*sim
                
                # Sum of similarities
                simSums.setdefault(item,0)
                simSums[item]+=sim                            
                
    # Create the normalized list
    rankings=[(total/simSums[item],item) for item,total in totals.items( )]

    # Return the sorted list
    rankings.sort( )
    rankings.reverse( )
    return rankings

def transformPrefs(prefs):
    result={}
    for person in prefs:
        for item in prefs[person]:
            result.setdefault(item,{})
            
            # Flip item and person
            result[item][person]=prefs[person][item]
    return result

def calculateSimilarItems(prefs,n=10):
    
    # Create a dictionary of items showing which other items they
    # are most similar to.
    result={}
    
    # Invert the preference matrix to be item-centric
    itemPrefs=transformPrefs(prefs)
    c=0
    for item in itemPrefs:
        
        # Status updates for large datasets
        c+=1
        #if c%100==0: print "%d / %d" % (c,len(itemPrefs))
            
        # Find the most similar items to this one
        scores=topMatches(itemPrefs,item,n=n,similarity=sim_distance)
        result[item]=scores
            
    return result

def getRecommendedItems(prefs,itemMatch,user):
    userRatings=prefs[user]
    scores={}
    totalSim={}
    
    # Loop over products rated by this user
    for (item,rating) in userRatings.items( ):
        
        # Loop over items similar to this one        
        for (similarity,item2) in itemMatch[item]:
            
            # Ignore if this user has already rated this product            
            if item2 in userRatings: continue
                
            # Weighted sum of rating times similarity            
            scores.setdefault(item2,0)
            scores[item2]+=similarity*rating
            
            # Sum of all the similarities
            totalSim.setdefault(item2,0)
            totalSim[item2]+=similarity
            
    # Divide each total score by total weighting to get an average
    rankings=[(score/totalSim[item],item) for item,score in scores.items( )]
    
    # Return the rankings from highest to lowest
    rankings.sort( )
    rankings.reverse( )
    return rankings

class Ecommerce:
    

    
    def __init__(self,f):
        self.fileName = f
    
    def LoadAndPreapereData(self):
        with open(self.fileName, "rb") as f:
            # read the entire input; in a real application,
            # you would want to read a chunk at a time
            bsondata = f.read()
            jsondata = re.sub(r'ObjectId\s*\(\s*\"(\S+)\"\s*\)',
                              r'"\1"',
                              bsondata)
            jsondata = re.sub(r'Date\s*\(\s*(\S+)\s*\)',
                              r'"\1"',
                              jsondata)
            jsondata1 = re.sub(r'"\\"TalkTalk\\""',
                              r'"TalkTalk"',
                              jsondata)
        
            l = len(jsondata1.splitlines())
            m = int(l/4000)

            self.df, self.customer, self.item_name, self.item_key = cleaning(jsondata1)
            
            self.__all_period__()
            self.__create_dict_names__()
            self.__preapere_data__()
    
    def __all_period__(self):
        # # Customer purchases per all period
        d = dict()
        k = list(zip(self.customer, self.item_key))
        for (x,y) in k:
            if x in d:
                d[x] = d[x] + y 
            else:
                d[x] = y

        self.order_d = collections.OrderedDict(sorted(d.items()))

        d = self.order_d
        val = d.values()
        for i,item in enumerate(val):
            val[i] = dict((key, len(list(group))) for key, group in groupby(sorted(item)))

        val[0]
        k = dict(zip(d.keys(), val))
        order_d1 = collections.OrderedDict(sorted(k.items()))
        self.d = order_d1

    def __create_dict_names__(self):
        # # Create dictionary with item's names

        dd = dict()
        k = list(zip(self.customer, self.item_name))
        for (x,y) in k:
            if x in dd:
                dd[x] = dd[x] + y 
            else:
                dd[x] = y

        names = collections.OrderedDict(sorted(dd.items()))

        a=self.order_d.values()
        b=names.values()

        self.gen = []
        for i in range(len(a)):
            for j in range(len(a[i])):
                self.gen.append((a[i][j], b[i][j]))

        self.gen = unique(self.gen)

        self.gen = dict(self.gen)
    def __preapere_data__(self):
        # # Results
        d1 = dict()
        k1 = list(zip(self.customer, self.item_name))
        for (x,y) in k1:
            if x in d1:
                d1[x] = d1[x] + y 
            else:
                d1[x] = y

        order_d1 = collections.OrderedDict(sorted(d1.items()))

        d1 = order_d1
        val1 = d1.values()
        for i,item in enumerate(val1):
            val1[i] = dict((key, len(list(group))) for key, group in groupby(sorted(item)))

        val1[0]
        k1 = dict(zip(d1.keys(), val1))
        order_d11 = collections.OrderedDict(sorted(k1.items()))
        d1 = order_d11
        self.products1 = transformPrefs(d1)

        self.prod_sim = calculateSimilarItems(self.d)
    
    # # list of 'you may also like' products on each product page
    def YouMayAlsoLike(self,fileName = 'YouMayAlsoLike.json',display = False):
        result = []
        for key in self.products1.keys():
            prod_list = topMatches(self.products1,key)
            if(key == None):
                continue
            if(display):
                print '------------------------------------------------------------------'
                print 'With \'{}\' you may also like: '.format(key.encode("utf-8"))
                print '------------------------------------------------------------------'
            for p in prod_list:
                t = p[1]
                
                if(display):
                    print t
                else:
                    result.append(t)
        if(not(display)):
            f = open(fileName, 'w')
            result = json.dumps(result)
            f.write(result)
            f.close()
    
    # # a list of upsells to recommend other products based on what is in a customers basket
    def RecommendProducts(self,basket,fileName = 'RecommendProducts.json',display = False):
        result = []
        if(display):
            print '------------------------------------------------------------------'
            print 'We recommend for you: '
            print '------------------------------------------------------------------'
        for k in basket:
            prod_list = topMatches(self.products1,k)
            for p in prod_list:
                if 'error' not in p[1] and 'discount' not in p[1] and '%' not in p[1]:
                    
                    if(display):
                        print p[1]
                    else:
                        result.append(p[1])
        if(not(display)):
            f = open(fileName, 'w')
            result = json.dumps(result)
            f.write(result)
            f.close()
                    
    # # a list of 'recommended for you' products based on what a customer has bought
    def RecommendedForYou(self,user,fileName = 'RecommendedForYou.json',display = False):
        result = []
        if(display):
            print '------------------------------------------------------------------'
            print 'We recommend for you: '
            print '------------------------------------------------------------------'
            
        r = getRecommendedItems(self.d, self.prod_sim, user)
        for i in range(len(r)):
            for key in self.gen.keys():
                if r[i][1] == key:
                    
                    if(display):
                        print self.gen[key]
                    else:
                        result.append(self.gen[key])
        if(not(display)):
            f = open(fileName, 'w')
            result = json.dumps(result)
            f.write(result)
            f.close()

    
if __name__ == '__main__':    
    e = Ecommerce("order.json")	
    e.LoadAndPreapereData()
    print '--------------------------------------------------'
    print '-------------Similarity calculating---------------'
    print '--------------------------------------------------'
    with open('products_list.txt', 'rU') as in_file:
        list_p = in_file.read().split('\n')
    customer = list_p[0]
    list_prod = list_p[1:]
    print 'Customer ', customer
    print 'List of products ', list_prod
    e.RecommendProducts(list_prod)
    print '--------------------------------------------------'
    print '-----------Reccomendations building---------------'
    print '--------------------------------------------------'
    e.RecommendedForYou(customer)
    print '--------------------------------------------------'
    print '------------------JOB IS DONE---------------------'
    print '--------------------------------------------------'
    print '--------Results are saved to the files------------'
    print '--------------------------------------------------'
    print '------------RecommendedForYou.json----------------'
    print '------------RecommendProducts.json----------------'
    print '--------------------------------------------------'