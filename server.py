
from BaseHTTPServer import HTTPServer
from CGIHTTPServer import CGIHTTPRequestHandler
from Ecommerce import Ecommerce
import resource
import json
import cgi
import csv
import os

class RequestHandler(CGIHTTPRequestHandler):
    e = Ecommerce("mongodb://127.0.0.1:27017/")
    e.LoadAndPreapereData()
    def cust_print(self, customer):
        cust = " "
        with open('LTV+cluster.csv', 'rt') as f:
             reader = csv.reader(f, delimiter=',')
             for row in reader:
                 for field in row:
                     if field == customer:
                        cust = row
                        print cust
        return cust

    def do_POST(self):
        ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
        if ctype == 'multipart/form-data':
            postvars = cgi.parse_multipart(self.rfile, pdict)
        elif ctype == 'application/x-www-form-urlencoded':
            length = int(self.headers.getheader('content-length'))
            postvars = cgi.parse_qs(self.rfile.read(length), keep_blank_values=1)
        else:
            postvars = {}
            
            
        
        try:
            if self.path == "/recommendedproducts":
                print '--------------------------------------------------'
                print '-------------Similarity calculating---------------'
                print '--------------------------------------------------'
                
                try:
                    list_prod = postvars['product_ids'][0].split("\r\n")
                except KeyError:
                    self.send_response(401)
                    self.send_header('Content-type', 'application/json')
                    self.end_headers()
                    self.wfile.write('[]')
                
                output = self.e.RecommendProducts(list_prod)

                print("MEMORY USAGE : %s kb" % resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)

                self.send_response(200)
                self.send_header('Content-type', 'application/json')
                self.end_headers()
                self.wfile.write(json.dumps(output))
                    

            elif self.path == "/foryou":
                print '--------------------------------------------------'
                print '-----------Reccomendations building---------------'
                print '--------------------------------------------------'
                
                
                try:
                    customer = postvars['customer'][0]
                except KeyError:
                    self.send_response(401)
                    self.send_header('Content-type', 'application/json')
                    self.end_headers()
                    self.wfile.write('[]')
                
                print("MEMORY USAGE : %s kb" % resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)

                output = self.e.RecommendedForYou(customer)

                self.send_response(200)
                self.send_header('Content-type', 'application/json')
                self.end_headers()
                self.wfile.write(json.dumps(output))
		self.wfile.write("\n")
		a = self.cust_print(customer)
		self.wfile.write(a)

            elif self.path == "/alsolike":
                # not working yet
                print '--------------------------------------------------'
                print '---------------Also like building-----------------'
                print '--------------------------------------------------'

                print("MEMORY USAGE : %s kb" % resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)

                output = self.e.YouMayAlsoLike()

                self.send_response(200)
                self.send_header('Content-type', 'application/json')
                self.end_headers()
                self.wfile.write(json.dumps(output))

	    elif self.path == "/groups":
                print '--------------------------------------------------'
                print '---------------Customer groups -------------------'
                print '--------------------------------------------------'
                os.system("python LTV+Cluster.py")
                self.send_response(200)
                self.end_headers()
                self.wfile.write("The output file with customer's groups is save in current folder with name 'LTV+cluster.csv")

        except Exception, e:
            print e
            self.send_response(500)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write("<h1>Server Error</h1>")
    
    # Show the form to do test posts
    def do_GET(self):
        if self.path == '/':
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            
            f = open('index.html', 'r')
            self.wfile.write(f.read())
        else:
            self.send_error(404, "Page '%s' not found (try /recommendedproducts)" % self.path)



def start():
    try:
        handler = RequestHandler
        server = HTTPServer(('', 8081), handler)
        print("Starting HTTP server")
        server.serve_forever()
        
    except KeyboardInterrupt:
        if server:
            server.socket.close()

if __name__ == '__main__':
    start()
