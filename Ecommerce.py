# coding: utf-8

from pymongo import MongoClient
import pandas as pd
import collections
from itertools import groupby
from math import sqrt
from contextlib import contextmanager
import sys


@contextmanager
def stdout_redirected(new_stdout):
    save_stdout = sys.stdout
    sys.stdout = new_stdout
    try:
        yield None
    finally:
        sys.stdout = save_stdout
        
def unique(s):
    
    n = len(s)
    if n == 0:
        return []
   
    u = {}
    try:
        for x in s:
            u[x] = 1
    except TypeError:
        del u  # move on to the next method
    else:
        return u.keys()
   
    try:
        t = list(s)
        t.sort()
    except TypeError:
        del t  # move on to the next method
    else:
        assert n > 0
        last = t[0]
        lasti = i = 1
        while i < n:
            if t[i] != last:
                t[lasti] = last = t[i]
                lasti += 1
            i += 1
        return t[:lasti]

    # Brute force is all that's left.
    u = []
    for x in s:
        if x not in u:
            u.append(x)
    return u

def sum_col(col):
    S = 0
    for n in col:
        S = S + n
    return S

def sim_distance(prefs,person1,person2):
    
    # Get the list of shared_items
    si={}
    for item in prefs[person1]:
        if item in prefs[person2]:
            si[item]=1
            
    # if they have no purchase in common, return 0
    if len(si)==0: return 0

    # Add up the squares of all the differences
    sum_of_squares=sum([pow(prefs[person1][item]-prefs[person2][item],2) for item in prefs[person1] if item in prefs[person2]])
    return 1.0/(1.0+float(sum_of_squares))

# Returns the Pearson correlation coefficient for p1 and p2
def sim_pearson(prefs,p1,p2):
    # Get the list of mutually rated items
    si={}
    for item in prefs[p1]:
        if item in prefs[p2]:
            si[item]=1

    # Find the number of elements
    n=len(si)

    # if they are no purchase in common, return 0
    if n==0: return 0

    # Add up all the preferences
    sum1=sum([prefs[p1][it] for it in si])
    sum2=sum([prefs[p2][it] for it in si])

    # Sum up the squares
    sum1Sq=sum([pow(prefs[p1][it],2) for it in si])
    sum2Sq=sum([pow(prefs[p2][it],2) for it in si])

    # Sum up the products
    pSum=sum([prefs[p1][it]*prefs[p2][it] for it in si])

    # Calculate Pearson score
    num=pSum-(sum1*sum2/n)
    den=sqrt((sum1Sq-pow(sum1,2)/n)*(sum2Sq-pow(sum2,2)/n))

    if den==0: return 0
    r=num/den
        
    return r

# Returns the best matches for person from the prefs dictionary.
# Number of results and similarity function are optional params.
def topMatches(prefs,person,n=5,similarity=sim_pearson):
    try:
        scores=[(similarity(prefs,person,other),other)  for other in prefs if other!=person]
    except KeyError:
        scores = []
    
    # Sort the list so the highest scores appear at the top
    scores.sort( )
    scores.reverse( )
    return scores[0:n]

# Gets recommendations for a person by using a weighted average
# of every other user's rankings
def getRecommendations(prefs,person,similarity=sim_pearson):
    totals={}
    simSums={}
    print '--------------------------------------------------'
    print '-----------Reccomandations building---------------'
    print '--------------------------------------------------'
    for other in prefs:
        
        # don't compare me to myself
        if other==person: 
            continue 
            
        try:
            sim=similarity(prefs,person,other)
        except KeyError:
            sim = 0
        
        # ignore scores of zero or lower
        if sim<=0: 
            continue
            
        for item in prefs[other]:
            # only score movies I haven't seen yet
            
            if item not in prefs[person] or prefs[person][item]==0:
                
                # Similarity * Score
                totals.setdefault(item,0)
                totals[item]+=prefs[other][item]*sim
                
                # Sum of similarities
                simSums.setdefault(item,0)
                simSums[item]+=sim                            
                
    # Create the normalized list
    rankings=[(total/simSums[item],item) for item,total in totals.items( )]

    # Return the sorted list
    rankings.sort( )
    rankings.reverse( )
    return rankings

def transformPrefs(prefs):
    result={}
    for person in prefs:
        for item in prefs[person]:
            result.setdefault(item,{})
            
            # Flip item and person
            result[item][person]=prefs[person][item]
    return result

def calculateSimilarItems(prefs,n=10):
    
    # Create a dictionary of items showing which other items they
    # are most similar to.
    result={}
    
    # Invert the preference matrix to be item-centric
    itemPrefs=transformPrefs(prefs)
    c=0
    for item in itemPrefs:
        
        # Status updates for large datasets
        c+=1
        #if c%100==0: print "%d / %d" % (c,len(itemPrefs))
            
        # Find the most similar items to this one
        scores=topMatches(itemPrefs,item,n=n,similarity=sim_distance)
        result[item]=scores
            
    return result

def getRecommendedItems(prefs,itemMatch,user):
    userRatings=prefs[user]
    scores={}
    totalSim={}
    
    # Loop over products rated by this user
    for (item,rating) in userRatings.items( ):
        
        # Loop over items similar to this one        
        for (similarity,item2) in itemMatch[item]:
            
            # Ignore if this user has already rated this product            
            if item2 in userRatings: continue
                
            # Weighted sum of rating times similarity            
            scores.setdefault(item2,0)
            scores[item2]+=similarity*rating
            
            # Sum of all the similarities
            totalSim.setdefault(item2,0)
            totalSim[item2]+=similarity
            
    # Divide each total score by total weighting to get an average
    rankings=[(score/totalSim[item],item) for item,score in scores.items( )]
    
    # Return the rankings from highest to lowest
    rankings.sort( )
    rankings.reverse( )
    return rankings

    
    
def cleaning(orders):
        
    print '--------------------------------------------------'
    print '-------------------Data reading-------------------'
    print '--------------------------------------------------'
    
    total = []
    sub_total = []
    customer = []
    date = []
    tax = []
    items1 = []
    items2 = []
    items3 = []
    items4 = []
    items5 = []
    items6 = []
    i = 0
    
    for order in orders:
        total.append(order['total'])
        sub_total.append(order['sub_total'])
        customer.append(order['customer'])
        date.append(order['created'])
        tax.append(order['tax'])
        if order['total'] > 0:
            items1.append(order['items'])
        else:
            items1.append('0')
        
        
    for i in range(len(items1)):
        name = []
        for j in range(len(items1[i])):
            if items1[i][j] > 0 and items1[i] != '0':            
                name.append(items1[i][j]['description'])
        items2.append(name)

    #for i in  range(len(items1)):
    #    name = []
    #    for j in range(len(items1[i])):
    #        if items1[i][j] > 0 and items1[i] != '0':            
    #            name.append(items1[i][j]['itemkey'])
    #    items3.append(name)

    #for i in  range(len(items1)):
    #    name = []
    #    for j in range(len(items1[i])):
    #        if items1[i][j] > 0 and items1[i] != '0':            
    #            name.append(items1[i][j]['total_price'])
    #    items4.append(name)

    #for i in  range(len(items1)):
    #    name = []
    #    for j in range(len(items1[i])):
    #        if items1[i][j] > 0 and items1[i] != '0':            
    #            name.append(items1[i][j]['qty'])
    #    items5.append(name)

    for i in  range(len(items1)):
        name = []
        for j in range(len(items1[i])):
            if items1[i][j] > 0 and items1[i] != '0':            
                name.append(items1[i][j]['productsku'])
        items6.append(name)
        
        
    print("READ : {:d} rows" . format(len(total)))
    # choose three parameters: total sum of check, customer id, date of buing
    
    # create Data Frame
    df = pd.DataFrame()
    
    #df['customer']=customer
    #df['date']=date
    #df['total']=total
    #df['sub_total'] =sub_total
    #df['tax']=tax
    #df['items_key']=items3
    #df['item_price']=items4
    #df['productsku']=items6
    #df['item_qty']=items5
    #df['items_descr']=items2

    return df, customer, items2, items6


class Ecommerce:
    
    def __init__(self, mongo_connect):
        self.mongo = MongoClient(mongo_connect)
    
    def init_df(self, orders_cur):
        self.df, self.customer, self.item_name, self.item_key = cleaning(orders_cur)
        # df not being used?
        self.df = None
    
    def LoadAndPreapereData(self):
        db = self.mongo['gorillaseedbank']
        self.init_df(db.order.find())
        
        self.__all_period__()
        #self.__create_dict_names__()
        self.__preapere_data__()
    
    def __all_period__(self):
        print '--------------------------------------------------'
        print '----------------Customer Purchases----------------'
        print '--------------------------------------------------'
        # # Customer purchases per all period
        d = dict()
        k = list(zip(self.customer, self.item_key))
        for (x,y) in k:
            if x in d:
                d[x] = d[x] + y 
            else:
                d[x] = y

        self.order_d = collections.OrderedDict(sorted(d.items()))

        d = self.order_d
        val = d.values()
        for i,item in enumerate(val):
            val[i] = dict((key, len(list(group))) for key, group in groupby(sorted(item)))

        val[0]
        k = dict(zip(d.keys(), val))
        order_d1 = collections.OrderedDict(sorted(k.items()))
        self.d = order_d1

    def __preapere_data__(self):
        print '--------------------------------------------------'
        print '-------------------Prepare Data-------------------'
        print '--------------------------------------------------'
        # # Results
        d1 = dict()
        k1 = list(zip(self.customer, self.item_key))
        for (x,y) in k1:
            if x in d1:
                d1[x] = d1[x] + y 
            else:
                d1[x] = y

        order_d1 = collections.OrderedDict(sorted(d1.items()))

        d1 = order_d1
        val1 = d1.values()
        for i,item in enumerate(val1):
            val1[i] = dict((key, len(list(group))) for key, group in groupby(sorted(item)))

        val1[0]
        k1 = dict(zip(d1.keys(), val1))
        order_d11 = collections.OrderedDict(sorted(k1.items()))
        d1 = order_d11
        self.products1 = transformPrefs(d1)

        self.prod_sim = calculateSimilarItems(self.d)
    
    # # list of 'you may also like' products on each product page
    def YouMayAlsoLike(self):
        result = []
        for key in self.products1.keys():
            prod_list = topMatches(self.products1,key)
            if(key == None):
                continue
            for p in prod_list:
                t = p[1]
                
                result.append(t)

        return result
    
    # # a list of upsells to recommend other products based on what is in a customers basket
    def RecommendProducts(self, basket):
        result = []
        
        for k in basket:
            prod_list = topMatches(self.products1, k)
            for p in prod_list:
                print p
                if 'error' not in p[1] and 'discount' not in p[1] and '%' not in p[1]:
                    result.append(p[1])
            
        return result
                    
    # # a list of 'recommended for you' products based on what a customer has bought
    def RecommendedForYou(self, user):
        result = []
        
        r = getRecommendedItems(self.d, self.prod_sim, user)
        for i in range(len(r)):
            result.append(r[i][1])
            
        return result
